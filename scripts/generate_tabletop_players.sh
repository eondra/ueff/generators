#!/bin/bash

source ${CONFIG_FOLDER}/general.conf

function rollRace() {
    # Roll characters race
    ROLL_RACE_CHANCE=$(shuf -i 1-100 -n 1)
    if (( $ROLL_RACE_CHANCE <= 25 ))
    then
        CHARACTER_RACE_INDEX=$(shuf -i 1-${#RACE_ARRAY[@]} -n 1)-1
        source ${CONFIG_FOLDER}/races/${RACE_ARRAY[$CHARACTER_RACE_INDEX]}.conf
        CHARACTER_RACE=${RACE_ARRAY[$CHARACTER_RACE_INDEX]}
        CHARACTER_RACE_MD=${RACE_NAME}
    else
        source ${CONFIG_FOLDER}/nations/${TEAM_NATION_LC}.conf
        CHARACTER_RACE=${NATION_PRIM_RACE}
        CHARACTER_RACE_MD=${NATION_PRIM_RACE_MD}
    fi
}

function rollNationality() {
    # Roll characters nationality
    ROLL_NATIONALITY_CHANCE=$(shuf -i 1-100 -n 1)

    if (( $ROLL_NATIONALITY_CHANCE <= 25 ))
    then
        CHARACTER_NATIONALITY_INDEX=$(shuf -i 1-${#NATIONS_ARRAY[@]} -n 1)-1
        source ${CONFIG_FOLDER}/nations/${NATIONS_ARRAY[$CHARACTER_NATIONALITY_INDEX]}.conf
        CHARACTER_NATIONALITY=${NATION_FULL_NAME_LOWERC}
        CHARACTER_NATIONALITY_MD=${NATION_FULL_NAME_MD}
        CHARACTER_NATIONALITY_INGAME=${NATION_CODE_INGAME}
    else
        source ${CONFIG_FOLDER}/nations/${TEAM_NATION_LC}.conf
        CHARACTER_NATIONALITY=${NATION_FULL_NAME_LOWERC}
        CHARACTER_NATIONALITY_MD=${NATION_FULL_NAME_MD}
        CHARACTER_NATIONALITY_INGAME=${NATION_CODE_INGAME}
    fi

    # Roll characters birthcity
    source ${CONFIG_FOLDER}/nations/${CHARACTER_NATIONALITY}.conf
    ROLL_CITY_INDEX=$(shuf -i 1-${#CITIES_ARRAY[@]} -n 1)-1
    CHARACTER_BIRTH_CITY_MD=${CITIES_ARRAY[$ROLL_CITY_INDEX]}
    CHARACTER_BIRTH_CITY_WEB=$(echo ${CITIES_ARRAY[$ROLL_CITY_INDEX]} | tr '[:upper:]' '[:lower:]')
}

function rollName() {
    # Generate names
    readarray -t CHARACTER_GIVEN_NAME < ${CONFIG_FOLDER}/namelists/${CHARACTER_RACE}/name_given_${CHARACTER_RACE}.txt
    readarray -t CHARACTER_LAST_NAME < ${CONFIG_FOLDER}/namelists/${CHARACTER_RACE}/name_last_${CHARACTER_RACE}.txt

    ROLL_CHARACTER_GIVEN_NAME=$(shuf -i 1-${#CHARACTER_GIVEN_NAME[@]} -n 1)-1
    ROLL_CHARACTER_LAST_NAME=$(shuf -i 1-${#CHARACTER_LAST_NAME[@]} -n 1)-1

    CHARACTER_GIVEN_NAME=${CHARACTER_GIVEN_NAME[$ROLL_CHARACTER_GIVEN_NAME]}
    CHARACTER_LAST_NAME=${CHARACTER_LAST_NAME[$ROLL_CHARACTER_LAST_NAME]}

    # In-game names need to be uppercase
    CHARACTER_GIVEN_NAME_INGAME=$(echo ${CHARACTER_GIVEN_NAME[$ROLL_CHARACTER_GIVEN_NAME]} | tr [a-z] [A-Z])
    CHARACTER_LAST_NAME_INGAME=$(echo ${CHARACTER_LAST_NAME[$ROLL_CHARACTER_LAST_NAME]} | tr [a-z] [A-Z])

    # For the website we want the first character of the name to be uppercase
    CHARACTER_GIVEN_NAME_MD=$(echo "${CHARACTER_GIVEN_NAME[$ROLL_CHARACTER_GIVEN_NAME]^}")
    CHARACTER_LAST_NAME_MD=$(echo "${CHARACTER_LAST_NAME[$ROLL_CHARACTER_LAST_NAME]^}")
}

function rollBirthday() {
    # Generate birthday
    source ${CONFIG_FOLDER}/races/${CHARACTER_RACE}.conf
    CHARACTER_AGE=$(shuf -i ${RACE_AGE_ADULT_START}-${RACE_AGE_ADULT_END} -n 1)

    # Make a proper birthday out of the age
    MONTH=$(shuf -i 1-12 -n 1)
    case $MONTH in
        1|3|5|7|8|10|12)
            DAY=$(shuf -i 1-31 -n 1)
            ;;
        4|6|9|11)
            DAY=$(shuf -i 1-30 -n 1)
            ;;
        2)
            DAY=$(shuf -i 1-28 -n 1)
            ;;
    esac

    # Larvic Calendar style
    YEAR=$(($CURRENT_YEAR - $CHARACTER_AGE))
    if (( $CURRENT_YEAR < $CHARACTER_AGE ))
    then
        YEAR=$(echo $YEAR | sed 's/-//g')
        YEAR="$YEAR BLC"
    else
        YEAR="$YEAR LC"
    fi
    CHARACTER_BIRTHDAY="$DAY.$MONTH.$YEAR"
}

function roll() {
    ROLL_SKILL=$(shuf -i 1-100 -n 1)
    if (($ROLL_SKILL<=2))
    then
        SKILL=8
    elif (($ROLL_SKILL<=5))
    then
        SKILL=9
    elif (($ROLL_SKILL<=25))
    then
        SKILL=10
    elif (($ROLL_SKILL<=55))
    then
        SKILL=11
    elif (($ROLL_SKILL<=80))
    then
        SKILL=12
    elif (($ROLL_SKILL<=95))
    then
        SKILL=13
    elif (($ROLL_SKILL<=98))
    then
        SKILL=14
    else
        SKILL=15
    fi
}

function rollAgility() {
    ROLL_SKILL=$(shuf -i 1-100 -n 1)
    if (($ROLL_SKILL<=55))
    then
        AGILITY=5
    elif (($ROLL_SKILL<=85))
    then
        AGILITY=6
    elif (($ROLL_SKILL<=95))
    then
        AGILITY=7
    else
        AGILITY=8
    fi
}

function rollSkill() {
    rollAgility
    roll
    GOALKEEPING=$SKILL
    roll
    DEFENDING=$SKILL
    roll
    HEADING=$SKILL
    roll
    PASSING=$(($SKILL+3))
    roll
    SHOOTING=$SKILL
    roll
    TECHNIQUE=$SKILL
    roll
    FITNESS=$SKILL
}

function determinePosition() {
    if [ $SHOOTING -gt 12 ]; then
        GOALKEEPING=$(($GOALKEEPING-5))
        DEFENDING=$(($DEFENDING-3))
        if [ $PASSING -le 12 ]; then
            PASSING=$(($PASSING+2))
        fi
        if [ $TECHNIQUE -le 12 ]; then
            TECHNIQUE=$(($TECHNIQUE+2))
        fi
        POSITION="ATTACKER"
        CHARACTER_POSITION_MD="Attacker"
    elif [ $DEFENDING -gt 12 ]; then
        GOALKEEPING=$(($GOALKEEPING-3))
        HEADING=$(($HEADING+3))
        if [ $PASSING -le 12 ]; then
            PASSING=$(($PASSING+2))
        fi
        SHOOTING=$(($SHOOTING-2))
        TECHNIQUE=$(($TECHNIQUE-1))
        POSITION="DEFENDER"
        CHARACTER_POSITION_MD="Defender"
    # Push Goalkeeping if SKILL is high
    elif [ $GOALKEEPING -gt 12 ]; then
        SHOOTING=$(($SHOOTING-5))
        HEADING=$(($HEADING-3))
        DEFENDING=$(($DEFENDING-1))
        TECHNIQUE=$(($TECHNIQUE-2))
        # A Goalkeeper should be able to pass accurately
        if [ $PASSING -le 13 ]; then
            PASSING=$(($PASSING+3))
        fi
        POSITION="GOALKEEPER"
        CHARACTER_POSITION_MD="Goalkeeper"
    elif [ $PASSING -gt 12 ]; then
        GOALKEEPING=$(($GOALKEEPING-5))
        if [ $HEADING -le 12 ]; then
            HEADING=$((HEADING+1))
        fi
        if [ $DEFENDING -le 12 ]; then
            DEFENDING=$(($DEFENDING+1))
        fi
        if [ $TECHNIQUE -le 12 ]; then
            TECHNIQUE=$(($TECHNIQUE+2))
        fi
        if [ $SHOOTING -le 12 ]; then
            SHOOTING=$(($SHOOTING+2))
        fi
        POSITION="MIDFIELDER"
        CHARACTER_POSITION_MD="Midfielder"
    else
        POSITION="?"
    fi
    WORTH_NET=$((($GOALKEEPING+$AGILITY+$DEFENDING+$HEADING+$PASSING+$SHOOTING+$TECHNIQUE+$FITNESS)))
}

# For Hugo website
function generatePlayerMD() {
    cat << EOF > ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/players/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}.md
---
author: "UEFF"
title: "$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD"
date:
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | $CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD |
| Date of birth | $CHARACTER_BIRTHDAY |
| City of birth | [$CHARACTER_BIRTH_CITY_MD](/cities/$CHARACTER_BIRTH_CITY_WEB) |
| Nationality | ![$CHARACTER_NATIONALITY_MD](/images/flags/${CHARACTER_NATIONALITY}_small_md.png) [$CHARACTER_NATIONALITY_MD](/nations/$CHARACTER_NATIONALITY) |
| Race | [$CHARACTER_RACE_MD](/races/$CHARACTER_RACE) |
| Position | $CHARACTER_POSITION_MD |
| Current club | [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) |

## Skills
| | |
|-|-|
| Goalkeeping | $GOALKEEPING |
| Defending | $DEFENDING |
| Heading | $HEADING |
| Passing | $PASSING |
| Technique | $TECHNIQUE |
| Agility | $AGILITY |
| Shooting | $SHOOTING |
| Fitness | $FITNESS |

## Club career

| Club | Season | Apps | Goals |
|-|-|-|-|
| [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) | $CURRENT_YEAR | | |

## Honours

EOF

    cat << EOF >> ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/index.md
| $CHARACTER_NUMBER |    $CHARACTER_POSITION_MD    | ![$CHARACTER_NATIONALITY_MD](/images/flags/${CHARACTER_NATIONALITY}_small_md.png) | [$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD](/characters/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}) | [$CHARACTER_RACE_MD](/races/$CHARACTER_RACE)     | $CHARACTER_BIRTHDAY |
EOF
}

function generateManager() {
    # Generate Manager
    rollRace
    rollName
    rollBirthday
    rollNationality
    TEAM_MANAGER_GIVEN_NAME=$CHARACTER_GIVEN_NAME_MD
    TEAM_MANAGER_LAST_NAME=$CHARACTER_LAST_NAME_MD
    TEAM_MANAGER_GIVEN_NAME_INGAME=$(echo $CHARACTER_GIVEN_NAME_MD | tr '[:lower:]' '[:upper:]')
    TEAM_MANAGER_LAST_NAME_INGAME=$(echo $CHARACTER_LAST_NAME_MD | tr '[:lower:]' '[:upper:]')
    TEAM_MANAGER_NATIONALITY=$CHARACTER_NATIONALITY
    TEAM_MANAGER_NATIONALITY_MD=$CHARACTER_NATIONALITY_MD

    # Generate markdown file for the Hugo website
    cat << EOF > ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/manager/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}.md
---
author: "UEFF"
title: "$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD"
date:
draft: false
type: "showcase"
---

## General info
| | |
|-|-|
| Name | $CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD |
| Date of birth | $CHARACTER_BIRTHDAY |
| City of birth | [$CHARACTER_BIRTH_CITY_MD](/cities/$CHARACTER_BIRTH_CITY_WEB) |
| Nationality | ![$CHARACTER_NATIONALITY_MD](/images/flags/${CHARACTER_NATIONALITY}_small_md.png) [$CHARACTER_NATIONALITY_MD](/nations/$CHARACTER_NATIONALITY) |
| Race | [$CHARACTER_RACE_MD](/races/$CHARACTER_RACE) |
| Position | Manager |
| Current club | [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) |

## Club career

| Club | Season | League position |
|-|-|-|
| [$TEAM_NAME_SHORT](/teams/$TEAM_NAME_STRING) | $CURRENT_YEAR | | |

## Honours

EOF
}

function generateSimulatorFile() {
    mkdir -p ${UEFF_FOLDER}/tabletop/${TEAM_NAME_STRING}/
    cat << EOF > ${UEFF_FOLDER}/tabletop/$TEAM_NAME_STRING/${CHARACTER_GIVEN_NAME}_${CHARACTER_LAST_NAME}
CHARACTER_NAME=$CHARACTER_GIVEN_NAME_MD $CHARACTER_LAST_NAME_MD
CHARACTER_POSITION=$POSITION
CHARACTER_AGILITY=$AGILITY
CHARACTER_GOALKEEPING=$GOALKEEPING
CHARACTER_DEFENDING=$DEFENDING
CHARACTER_HEADING=$HEADING
CHARACTER_PASSING=$PASSING
CHARACTER_SHOOTING=$SHOOTING
CHARACTER_TECHNIQUE=$TECHNIQUE
CHARACTER_FITNESS=$FITNESS
CHARACTER_WORTH=$WORTH_NET
EOF
}

function main() {
    if [[ $GEN_MANAGER = 1 ]];then
        generateManager
        export TEAM_MANAGER_GIVEN_NAME TEAM_MANAGER_LAST_NAME TEAM_MANAGER_GIVEN_NAME_INGAME TEAM_MANAGER_LAST_NAME_INGAME TEAM_MANAGER_NATIONALITY TEAM_MANAGER_NATIONALITY_MD
        GEN_MANAGER=0
    fi

    if [[ $GEN_CHARACTERS = 1 ]]; then
        CHARACTERS_GENERATED=0
        CHARACTER_NUMBER=1

        for (( POSITION_COUNTER=0 ; POSITION_COUNTER <= ${#SIM_REQ_NUM_POSITION[@]} ; ++POSITION_COUNTER ))
        do
            while [[ ! -z ${SIM_REQ_NUM_POSITION[$POSITION_COUNTER]} && ${SIM_REQ_NUM_POSITION[$POSITION_COUNTER]} -gt $CHARACTERS_GENERATED ]]
            do
                rollRace
                rollName
                rollBirthday
                rollNationality
                rollAgility
                rollSkill
                determinePosition
                if [ "$POSITION" = "${SIM_POSITION_ARRAY[$POSITION_COUNTER]}" ]; then
                    generatePlayerMD
                    generateSimulatorFile
                    CHARACTERS_GENERATED=$(($CHARACTERS_GENERATED+1))
                    CHARACTER_NUMBER=$(($CHARACTER_NUMBER+1))
                fi
            done
            CHARACTERS_GENERATED=0
        done
    fi
}

main
