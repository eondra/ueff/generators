#!/bin/bash

source ${CONFIG_FOLDER}/general.conf

function cliOutput() {
    echo "$CLIMESSAGE"
}

function getUserInput() {
    read -p "Enter the teams full name (case sensitive): " TEAM_NAME_FULL
    read -p "Enter the teams short name (case sensitive): " TEAM_NAME_SHORT
    read -p "Enter the teams nation (case sensitive): " TEAM_NATION
    read -p "Enter the teams city (case sensitive): " TEAM_CITY
    read -p "Enter the teams stadium name (case sensitive): " TEAM_STADIUM_NAME
    read -p "Enter the teams founding date (Format: dd/mm/yy LC): " TEAM_FOUND_DATE
}

function formatStrings() {
    # TEAM_NAME_STRING is for our directory/file name. NO SPACES! and lower case
    TEAM_NAME_STRING=$(echo "$TEAM_NAME_SHORT" | tr '[:upper:]' '[:lower:]' | sed 's/ //g')
    # All In-game strings need to be upper case
    TEAM_NAME_INGAME=$(echo "$TEAM_NAME_SHORT" | tr '[:lower:]' '[:upper:]')
    TEAM_STADIUM_INGAME=$(echo "$TEAM_STADIUM_NAME" | tr '[:lower:]' '[:upper:]')
    TEAM_CITY_INGAME=$(echo "$TEAM_CITY" | tr '[:lower:]' '[:upper:]')
    TEAM_NATION_CODE=$(echo "$TEAM_NATION" | cut -c1-3 | tr '[:lower:]' '[:upper:]')
    TEAM_NATION_LC=$(echo "$TEAM_NATION" | tr '[:upper:]' '[:lower:]')
}

function createFolders() {
    CLIMESSAGE="Generating folders"
    cliOutput
    mkdir -p ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}/players
    mkdir -p ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}/manager
}

# For ySoccer
function generateKits() {
    CLIMESSAGE="Generating kits"
    cliOutput
    # Generates kits out of the color array in loadcolors()
    ROLL_KIT_COLOR_1=$(shuf -i 1-${#COLOR_ARRAY[@]} -n 1)-1
    ROLL_KIT_COLOR_2=$(shuf -i 1-${#COLOR_ARRAY[@]} -n 1)-1
    ROLL_KIT_COLOR_3=$(shuf -i 1-${#COLOR_ARRAY[@]} -n 1)-1
    # Make sure we have at least 2 different colors
    while [ "${COLOR_ARRAY[$ROLL_KIT_COLOR_3]}" == "${COLOR_ARRAY[$ROLL_KIT_COLOR_1]}" ] || [ "${COLOR_ARRAY[$ROLL_KIT_COLOR_3]}" == "${COLOR_ARRAY[$ROLL_KIT_COLOR_2]}" ]
    do
        ROLL_KIT_COLOR_3=$(shuf -i 1-${#COLOR_ARRAY[@]} -n 1)-1
    done

    TEAM_KIT_COLOR_1=${COLOR_ARRAY[$ROLL_KIT_COLOR_1]}
    TEAM_KIT_COLOR_2=${COLOR_ARRAY[$ROLL_KIT_COLOR_2]}
    TEAM_KIT_COLOR_3=${COLOR_ARRAY[$ROLL_KIT_COLOR_3]}

    # Generates kit styles out of the kitstyles array
    ROLL_KIT_STYLE_1=$(shuf -i 1-${#KIT_STYLES_ARRAY[@]} -n 1)-1
    ROLL_KIT_STYLE_2=$(shuf -i 1-${#KIT_STYLES_ARRAY[@]} -n 1)-1
    # Make the 2nd style the 3rd style
    ROLL_KIT_STYLE_3=$ROLL_KIT_STYLE_2

    TEAM_KIT_STYLE_1=${KIT_STYLES_ARRAY[$ROLL_KIT_STYLE_1]}
    TEAM_KIT_STYLE_2=${KIT_STYLES_ARRAY[$ROLL_KIT_STYLE_2]}
    TEAM_KIT_STYLE_3=${KIT_STYLES_ARRAY[$ROLL_KIT_STYLE_3]}
}

# For ySoccer
function generateFormation() {
    CLIMESSAGE="Generating formation"
    cliOutput
    # Generate formation/tactic for the team out of the current supported ones
    ROLL_TEAM_FORMATION=$(shuf -i 1-${#FORMATION_ARRAY[@]} -n 1)-1
    TEAM_FORMATION=${FORMATION_ARRAY[$ROLL_TEAM_FORMATION]}
}

function generateManager() {
    CLIMESSAGE="Generating manager"
    cliOutput
    export TEAM_NATION_LC TEAM_NAME_SHORT TEAM_NAME_STRING GEN_MANAGER=1
    . ${SCRIPT_FOLDER}/${CHARACTER_GENERATOR_SCRIPT}
    GEN_MANAGER=0
}

# For Hugo website
function generateTeamMD() {
    CLIMESSAGE="Generating markdown file"
    cliOutput
    # Generate markdown file of the team for the website
    cat << EOF > ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}/index.md
---
author: "UEFF"
title: "$TEAM_NAME_SHORT"
date:
draft: false
type: "showcase"
---

## General info

| | |
|-|-|
| Name | $TEAM_NAME_FULL |
| Nation | [$TEAM_NATION](/nations/${TEAM_NATION_LC}) |
| City | $TEAM_CITY |
| Founded | $TEAM_FOUND_DATE |
| Field | $TEAM_STADIUM_NAME |
| Coach | ![$TEAM_MANAGER_NATIONALITY_MD](/images/flags/${TEAM_MANAGER_NATIONALITY}_small_md.png) | [$TEAM_MANAGER_GIVEN_NAME $TEAM_MANAGER_LAST_NAME](/characters/${TEAM_MANAGER_GIVEN_NAME}_${TEAM_MANAGER_LAST_NAME})


## Current squad

|  # | Position | |          Name             |  Race   | Birthday |
|:--:|:--------:|-|:-------------------------:|:-------:|:--------:|
EOF
}

# For ySoccer
function generateTeamJSON() {
    CLIMESSAGE="Generating JSON file"
    cliOutput
    cat << EOF > ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/team.$TEAM_NAME_STRING.json
{
"name": "$TEAM_NAME_INGAME",
"type": "CUSTOM",
"country": "$TEAM_NATION_CODE",
"city": "$TEAM_CITY_INGAME",
"stadium": "$TEAM_STADIUM_INGAME",
"coach": {
    "name": "$TEAM_MANAGER_GIVEN_NAME_INGAME $TEAM_MANAGER_LAST_NAME_INGAME",
    "nationality": "
},
"tactics": "$TEAM_FORMATION",
"kits": [
	{
    "style": "$TEAM_KIT_STYLE_1"
    "shirt1": "$TEAM_KIT_COLOR_1",
    "shirt2": "$TEAM_KIT_COLOR_2",
    "shirt3": "$TEAM_KIT_COLOR_3",
    "shorts": "$TEAM_KIT_COLOR_2",
    "socks": "$TEAM_KIT_COLOR_1"
  },
  {
    "style": "$TEAM_KIT_STYLE_2"
    "shirt1": "$TEAM_KIT_COLOR_2",
    "shirt2": "$TEAM_KIT_COLOR_1",
    "shirt3": "$TEAM_KIT_COLOR_3",
    "shorts": "$TEAM_KIT_COLOR_1",
    "socks": "$TEAM_KIT_COLOR_2"
  },
  {
    "style": "$TEAM_KIT_STYLE_3"
    "shirt1": "$TEAM_KIT_COLOR_3",
    "shirt2": "$TEAM_KIT_COLOR_1",
    "shirt3": "$TEAM_KIT_COLOR_2",
    "shorts": "$TEAM_KIT_COLOR_1",
    "socks": "$TEAM_KIT_COLOR_3"
  }
],
"players": [
EOF
}

function generatePlayers() {
    CLIMESSAGE="Generating characters"
    cliOutput
    export TEAM_NATION_LC TEAM_NAME_SHORT TEAM_NAME_STRING GEN_CHARACTERS=1
    . ${SCRIPT_FOLDER}/${CHARACTER_GENERATOR_SCRIPT}
    GEN_CHARACTERS=0
}

# For ySoccer
function finishTeamJSON(){
    CLIMESSAGE="Finishing JSON file"
    cliOutput
    cat << EOF >> ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/team.$TEAM_NAME_STRING.json
]
}
EOF
}

# For Hugo website
function finishTeamMD() {
    CLIMESSAGE="Finishing markdown file"
    cliOutput
    cat << EOF >> ${UEFF_FOLDER}/generated-teams/$TEAM_NAME_STRING/index.md

## Honours

EOF
}

function finalOutput() {
    CLIMESSAGE="Your team has been saved at ${UEFF_FOLDER}/generated-teams/${TEAM_NAME_STRING}"
    cliOutput
}

function main() {
    getUserInput
    formatStrings
    createFolders
    generateKits
    generateFormation
    generateManager
    generateTeamMD
    generateTeamJSON
    generatePlayers
    finishTeamJSON
    finishTeamMD
    finalOutput
}

main
