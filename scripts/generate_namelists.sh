#!/bin/bash

source ${CONFIG_FOLDER}/general.conf

GIVEN_TEMP=$(mktemp)
LAST_TEMP=$(mktemp)

NAME_GIVEN=$(mktemp)
NAME_LAST=$(mktemp)

LOWER_NAME_GIVEN=$(mktemp)
LOWER_NAME_LAST=$(mktemp)

SORTED_NAME_GIVEN=$(mktemp)
SORTED_NAME_LAST=$(mktemp)

createNamelist() {
    for (( i=0 ; i <= ${#RACE_ARRAY[@]} -1 ; ++i ))
        do
            echo "Generating ${RACE_ARRAY[i]} namelist."
            cp ${CONFIG_FOLDER}/namelists/${RACE_ARRAY[i]}/name_list_${RACE_ARRAY[i]}.txt $GIVEN_TEMP
            cp ${CONFIG_FOLDER}/namelists/${RACE_ARRAY[i]}/name_list_${RACE_ARRAY[i]}.txt $LAST_TEMP

            # Cut names
            cut -d ' ' -f 1 < $GIVEN_TEMP > ${NAME_GIVEN}_${RACE_ARRAY[i]}.txt
            sed 's/[^ ]* //' $LAST_TEMP >  ${NAME_LAST}_${RACE_ARRAY[i]}.txt

            # lower case names
            tr [A-Z] [a-z] < ${NAME_GIVEN}_${RACE_ARRAY[i]}.txt > ${LOWER_NAME_GIVEN}_${RACE_ARRAY[i]}.txt
            tr [A-Z] [a-z] < ${NAME_LAST}_${RACE_ARRAY[i]}.txt > ${LOWER_NAME_LAST}_${RACE_ARRAY[i]}.txt

            # erase duplicates
            sort -u ${LOWER_NAME_GIVEN}_${RACE_ARRAY[i]}.txt > ${SORTED_NAME_GIVEN}_${RACE_ARRAY[i]}.txt
            sort -u ${LOWER_NAME_LAST}_${RACE_ARRAY[i]}.txt > ${SORTED_NAME_LAST}_${RACE_ARRAY[i]}.txt

            # copy to folder
            cp ${SORTED_NAME_GIVEN}_${RACE_ARRAY[i]}.txt ${CONFIG_FOLDER}/namelists/${RACE_ARRAY[i]}/${NAME_GIVEN}_${RACE_ARRAY[i]}.txt
            cp ${SORTED_NAME_LAST}_${RACE_ARRAY[i]}.txt ${CONFIG_FOLDER}/namelists/${RACE_ARRAY[i]}/${NAME_LAST}_${RACE_ARRAY[i]}.txt
        done
}

cleanup() {
    rm $GIVEN_TEMP $LAST_TEMP $NAME_GIVEN $NAME_LAST $LOWER_NAME_GIVEN $LOWER_NAME_LAST $SORTED_NAME_GIVEN $SORTED_NAME_LAST
}

main() {
    createNamelist
    cleanup
    echo "DONE"
    echo "Namelists saved: ${CONFIG_FOLDER}/namelists"
}

main